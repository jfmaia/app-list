# ChallengeApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Hosting

This project is hosted on firebase. To access the live version navigate to `https://talksdesk-challenge.firebaseapp.com/`.

To accomplish this it was needed to add firebase to the project with `ng add @angular/fire@next`, add `"deploy": "ng run challenge-app:deploy"` to package.json and run ng run challenge-app:deploy.

# Implementation

To get the data in the json file:
- It was created a service: 'data.service.ts' where it is able to get all data.
- The data processing is done on products component, inside products.component.ts.

- getData() => If the json reading is successful, the data is ordered by price and filtered in categories (if selected).

- orderByPrice(json: App[]) => Sort json by the sum of the item prices.

- getNumberPages(json: App[]) => Get the available number of pages according to the filteredJSON.

- filteredData() => Filter json according to the category selected. After the filtered json is generated a new recalculation of the pages is made with getNumberPages().

- searchData(search: string) => Using reactive forms it is able to know when the search value changes. When the search name matches the app name, the json is filtered.

# Routing

Used for paginating and to search by category.