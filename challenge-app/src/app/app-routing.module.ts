import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChannelsComponent } from './products/products.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'products/1' },
  { path: 'products', pathMatch: 'full', redirectTo: 'products/1' },
  { path: 'products/:category/:id', component: ChannelsComponent },
  { path: 'products/:id', component: ChannelsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
