import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { App } from 'src/assets/interfaces/interface';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-channels',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ChannelsComponent implements OnInit {

  dataJSON: App[] = [];
  dataPaginated: App[] = [];
  menuItems: string[] = ['Channels', 'Reporting', 'Optimization', 'Dialer', 'Voice Analytics'];
  numberPages: number[] = [];
  currentPage: number;
  category: string;
  filteredJSON: App[] = [];

  searchField:FormControl =  new FormControl();
  search: string;

  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['id']) {
        this.currentPage = Number(params['id']);
      }
      if (params['category']) {
        this.category = params['category'];
      }
      this.filteredData();
    });

    this.getData();
    this.searchField.valueChanges.subscribe(term => {
      this.search = term;
      this.searchData(this.search);
    });
  }

  private getData() {
    this.dataService.getJSON().subscribe(
      (result) => {
        this.dataJSON = result;
        this.orderByPrice(this.dataJSON);
        this.filteredData();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private orderByPrice(json: App[]) {
    json.sort(function (a, b) {
      let aprice = 0;
      let bprice=0;
      a.subscriptions.forEach(element => {
        aprice+=element.price;
      });
      b.subscriptions.forEach(element => {
        bprice+=element.price
      });
      return aprice-bprice;
    });
  }

  getNumberPages(json: App[]) {
    this.numberPages = [];
    const number = Math.ceil(json.length / 3);
    for (let i = 0; i < number; i ++) {
      this.numberPages.push(i);
    }
  }

  private filteredData() {
    this.filteredJSON = [];
    const json = this.dataJSON;
    if (this.category) {
      json.forEach(app => {
        app.categories.forEach(element => {
          if (element === this.category) {
            this.filteredJSON.push(app);
          }
        });
      });
    } else {
      this.filteredJSON = json;
    }
    this.getNumberPages(this.filteredJSON);
  }

  private searchData(search: string) {
    this.filteredData();
    const json = [];
    for (let i = 0; i < this.filteredJSON.length; i ++){
      if(this.filteredJSON[i].name.toLowerCase().indexOf(search) != -1) {
        json.push(this.filteredJSON[i]);
      }
    }
    this.filteredJSON = json;
    this.getNumberPages(this.filteredJSON);
  }
}
