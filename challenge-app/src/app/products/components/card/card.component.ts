import { Component, OnInit, Input } from '@angular/core';
import { App } from 'src/assets/interfaces/interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() app: App[];

  constructor() { }

  ngOnInit() {
  }

}
