import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { App } from 'src/assets/interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private jsonFile = './assets/json/apps.json';

  constructor(private http: HttpClient) { }

  public getJSON(): Observable<App[]> {
    return this.http.get<App[]>(this.jsonFile);
  }
}
