export interface App {
    id: string;
    name: string;
    description: string;
    categories: string[];
    subscriptions: Subscriptions[];
}

export interface Subscriptions {
    name: string;
    price: number;
}